public class Exercise1 {
    public static void main(String[] args) {
        //test
    }
}
// nu am mai specificat constructorii fara parametrii unde nu era necesar fiind impliciti
class C {
}

class B{
    public void metB(){
    }
}

class A extends C{
    private M m;
    public A(){
        this.m = new M();
    }

    public void metA(){
    }
}

class M{
    private B b;

    public M(B b){
        this.b = b;
    }

    public M() {
    }
}

class L{
    private int a;
    private M m;
    public L(M m){
        this.m = m;
    }
    public void i (X x) {
    }
}

class X{

}