

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Exercise2{
    public static void main(String[] args) {
        Window window = new Window();
    }
}

class Window extends JFrame{
    private JButton button = new JButton("Click!");
    private JTextField tField1 = new JTextField();
    private JTextField tField2 = new JTextField();
    private JTextField tField3 = new JTextField();
    private JLabel label = new JLabel();
    public Window(){
        setSize(800,400);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        getContentPane().setLayout(null);
        tField1.setBounds(50,40,200,50);
        tField2.setBounds(300,40,200,50);
        tField3.setBounds(550,40,200,50);
        tField3.setEditable(false);
        label.setBounds(100,20,600,18);
        label.setText("Input text in the 2 text fields and the sum of characters from both fields will show up in the 3rd one");
        button.setBounds(330,200,140,140);
        button.addActionListener(new MyActionListener(this));
        add(button); add(tField1); add(tField2); add(tField3); add(label);
        setVisible(true);
    }
    public String getTextField1(){

        return String.valueOf(tField1.getText());
    }
    public String getTextField2(){

        return String.valueOf(tField2.getText());
    }
    public void setTextField3(int n){
        tField3.setText(String.valueOf(n));
    }

}
class MyActionListener implements ActionListener {
    Window window ;
    public MyActionListener(Window window){
        this.window = window;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        String s = window.getTextField1() + window.getTextField2();
        window.setTextField3(s.length());
    }
}